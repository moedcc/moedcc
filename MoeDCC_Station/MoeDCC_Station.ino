/*
    This file is part of MoeDCC
    Copyright (C) 2014  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//work started at 2014-01-24 20:54


/*
Sources:
http://www.dccwiki.com/Digital_packet
http://opendcc.de/elektronik/opendecoder/opendecoder_sw_dcc.html
http://www.nmra.org/standards/DCC/standards_rps/S-92-2004-07.pdf

*/

#include <EEPROM.h>

//NOTE: delayMicroseconds must be very slow!
#define DCC_ONE_LENGTH 22 //produces 17.52 kHz
#define DCC_ZERO_LENGTH 43 //produces 10.06 kHz with this settings

/*
#define DCC_ONE_LENGTH 58 //produces 7.67 kHz
#define DCC_ZERO_LENGTH 100 //produces 4.653 kHz
*/

#define DCC_OUTPIN 5

unsigned char sendBit = 0;

/**
  This function sends one DCC one.
**/
inline void sendOne()
{
  digitalWrite(DCC_OUTPIN, 1);
  delayMicroseconds(DCC_ONE_LENGTH);
  digitalWrite(DCC_OUTPIN, 0);
  delayMicroseconds(DCC_ONE_LENGTH);  
}

/**
  This function sends one DCC zero.
**/
inline void sendZero()
{
  digitalWrite(DCC_OUTPIN, 1);
  delayMicroseconds(DCC_ZERO_LENGTH);
  digitalWrite(DCC_OUTPIN, 0);
  delayMicroseconds(DCC_ZERO_LENGTH);
}


//void DCCSignals[2](); //array of functions with length 2 (NOT WORKING)
//DCCSignals[0] = sendZero;
//DCCSignals[1] = sendOne;


void sendZeroIdle()
{
  for(unsigned char i = 0; i < 25; i++)
  {
    sendZero();
  }
}



void sendIdle()
{
  //the idle signal consists of 12 ones, followed by one zero, 8 ones, 10 zeros and 9 ones:
  //111111111111 0 11111111 0 00000000 0 11111111 1
  
  for(unsigned char i = 0; i < 12; i++)
  {
    sendOne();
  }
  
  sendZero();
  
  for(unsigned char i = 0; i < 8; i++)
  {
    sendOne();
  }
  
  for(unsigned char i = 0; i < 10; i++)
  {
    sendZero();
  }
  
  for(unsigned char i = 0; i < 9; i++)
  {
    sendOne();
  }
}


void sendPacket(unsigned char address, unsigned char instruction)
{
  //calculate error byte:
  unsigned char error = address ^ instruction; //address XOR instruction
 
 //start sending:
 
  //preamble
  for(unsigned char i = 0; i < 12; i++)
  {
    sendOne();
  }
  
  sendZero();
  
  //address (send the MSB first):
  for(char i = 7; i >= 0; i--)
  {
    unsigned char currentBit = (address >> i) & 0x01;
    if(currentBit == 1)
    {
      sendOne();
    }
    else
    {
      sendZero();
    }
  }
  
  sendZero();
  
  //instruction (send the MSB first):
  for(char i = 7; i >= 0; i--)
  {
    unsigned char currentBit = (instruction >> i) & 0x01;
    if(currentBit == 1)
    {
      sendOne();
    }
    else
    {
      sendZero();
    }
  }
  
  sendZero();
  
  //error correction byte (send the MSB first):
  for(char i = 7; i >= 0; i--)
  {
    unsigned char currentBit = (error >> i) & 0x01;
    if(currentBit == 1)
    {
      sendOne();
    }
    else
    {
      sendZero();
    }
  }
  
  sendOne();
  
}


void receiveCommand()
{
  //read command from serial:
  char commandString[33];
  Serial.readBytesUntil('\n',commandString, 32);
  
  
  char command = 0;
  int address = 0;
  int parameter = 0;
  
  sscanf(commandString, "%c %d %d\n", &command, &address, &parameter);
  
  //DEBUG output via serial:
  
  Serial.println("DEBUG\n--------\nRead command:");
  Serial.print("Command: ");
  Serial.println(command);
  Serial.print("Address: ");
  Serial.println(address);
  Serial.print("Parameter: ");
  Serial.println(parameter);
  Serial.println("--------\nEND DEBUG");
  
  unsigned char instruction = 0x40; //=0b0100 0000;
  
  switch (command)
  {
    case 's':
    {
      //speed command: the parameter holds the speed step (0-14)
      if(parameter > 14 || parameter < -14)
      {
        Serial.println("ERROR: speed step out of range (-14 to 14)!");
      }
      else
      {
        //01DCSSSS;
        if(parameter < 0)
        {
          unsigned char speedStep = (unsigned char)(parameter*(-1));
          
          //do not set direction bit since the loco should move backwards:
          //bits 0100 SSSS
          instruction = 0x40 | (speedStep & 0x0F);
        }
        else
        {
          unsigned char speedStep = (unsigned char)parameter;
          //set direction bit since the loco should move forward:
          //bits 0110 SSSS
          instruction = 0x60 | (speedStep & 0x0F);
        }
        
        digitalWrite(13,0);
        for(unsigned int i = 0; i < 2000; i++)
        {
          
          sendPacket(address, instruction);
          sendZeroIdle();
        }
        digitalWrite(13,1);
      }
    }
    
  }
  
}


void setup()
{
  //set pins:
  pinMode(DCC_OUTPIN, OUTPUT);
  pinMode(13, OUTPUT); //blink the LED for status report
  
  //system operational message:
  digitalWrite(13,1);
  delay(100);
  digitalWrite(13,0);
  delay(500);
  digitalWrite(13,1);
  
  Serial.begin(9600);
}

void loop()
{
  
  while(true)
  {
    while(!Serial.available())
    {
      sendIdle();
      sendZeroIdle(); //sends zero bits for 5 milliseconds
    }
    //serial data is available here
    receiveCommand();
    Serial.flush();
  }
  
  //Test:
  
  while(true)
  {
    //sendZero();
    //sendOne();
  }
}
