/*
    This file is part of MoeDCC
    Copyright (C) 2014  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//work started at 2014-01-13 22:37


/*
Sources:
http://www.dccwiki.com/Digital_packet
http://opendcc.de/elektronik/opendecoder/opendecoder_sw_dcc.html
http://www.nmra.org/standards/DCC/standards_rps/S-92-2004-07.pdf

*/

#include <EEPROM.h>


int motorForwardPin = 5;
int motorReversePin = 6;
int DCCInputPin = 2;

#define STATUS_LED 3
#define DEBUG_LED 4

static unsigned char minimumPWM = 184; //the minimum value to be used for PWM


unsigned char myAddress = 0; //address 0 is not possible in DCC: Range is from 1 to 127

unsigned char currentSpeed = 0; //the current speed of the loco
unsigned char currentDirectionForward = 1; //1 if forward, 0 if reverse


class InterruptReceivingStatus
{
  public:
    unsigned char preambleSize;
    unsigned char address;
    unsigned char instruction;
    unsigned char error;
    unsigned char receiveState; //0 = before preamble, 1 = inside preamble, 2 = inside address, 3 = inside instruction, 4 = inside error
    unsigned char dataBitsRead = 0; //counts the bits for address, instruction and error
    void reset()
    {
      this->preambleSize = 0;
      this->address = 0;
      this->instruction = 0;
      this->error = 0;
      this->receiveState = 0;
      this->dataBitsRead = 0;
    }
};


InterruptReceivingStatus iStatus;

/**
  This function is responsible for setting the speed step. At the moment only the 14 speed step mode is implemented properly.
  @param speed The speed parameter has possible values from 0 to 255. In DCC only values from 0 to 31 are used
               1 and 17 is emergency break. 0 and 16 is stop.
**/
void setSpeed(unsigned char speed)
{
  if((speed == 1) || (speed == 17) || (speed == 0) || (speed == 16))
  {
    //emergency break not yet implemented (neither in software nor hardware)
    //so just stop providing power to the motor, like when speed 0 is selected:
    analogWrite(motorForwardPin,0);
    analogWrite(motorReversePin,0);
    currentSpeed = 0;
  }
  else
  {
    unsigned char transformedSpeed = 0; //transform the speed value from 2-15 to $minimumPWM-255
    
    unsigned char speedRange = 255 - minimumPWM;
    
    //NORMAL:
    //transformedSpeed = ((speed - 1)/14)*speedRange + minimumPWM;
    //currentSpeed = transformedSpeed;
    
    //DEBUG:
    currentSpeed = 120 + (10*(speed-1));
    
    if(currentDirectionForward == 1)
    {
      analogWrite(motorForwardPin, currentSpeed);
      analogWrite(motorReversePin, 0);
    }
    else
    {
      analogWrite(motorForwardPin, 0);
      analogWrite(motorReversePin, currentSpeed);
    }
  }
}


/**
  This function is responsible for decoding the DCC instruction byte 
**/

void executeCommand(unsigned char instruction)
{
  //the instruction byte is separated into different areas:
  //the bits 7 and 6 contain the bits 0 and 1.
  //bit 5 is the direction bit
  //bit 4 either the headlight bit or the MSB of the speed
  //bits 0-3 store the speed
  
  //unsigned char speed = instruction & 0x1F; //for 28 speed steps
  unsigned char speed = instruction & 0x0F; //for 14 speed steps from 2 to 14 (plus zero for off)
  unsigned char headlight = (instruction & 0x10) >> 4; //only useful for 14 speed steps
  unsigned char direction = (instruction & 0x20) >> 5; //the direction bit
  
  unsigned char bits01 = (instruction & 0xC0) >> 6;
  //NORMAL:
  //if(bits01 == 1)
  //DEBUG:
  if(bits01 = 1)
  {
    digitalWrite(STATUS_LED,0);
    //this is an instruction we understand
    currentDirectionForward = direction;
    setSpeed(speed);
    digitalWrite(STATUS_LED,1);
  }
  
}


void pushBit(unsigned char receivedBit)
{
  //DEBUG:
  if(iStatus.receiveState == 3)
  {
    digitalWrite(DEBUG_LED, 1);
  }
  else
  {
    digitalWrite(DEBUG_LED, 0);
  }
  //END DEBUG
  
  switch(iStatus.receiveState)
  {
    digitalWrite(STATUS_LED, 0);
    case 0:
    {
      //before preamble:
      if(receivedBit == 1)
      {
        //count the one bits in the preamble
        iStatus.receiveState++;
        iStatus.preambleSize++;
      }
      break;
    }
    case 1:
    {
      //inside the preamble
      if(receivedBit == 1)
      {
        //a one received: count it
        iStatus.receiveState++;
      }
      else
      {
        //a zero has been received, possibly the zero bit after the preamble, we'll check for that now:
        if(iStatus.preambleSize == 12)
        {
          //the preamble size is 12, so a correct preamble, followed by a zero has been read
          //we can move on to the next state and read the address:
          iStatus.receiveState++;
        }
        else
        {
          //the preamble size is not right, start from the beginning:
          iStatus.reset();
        }
      }
      
      break;
    }
    
    case 2:
    {
      digitalWrite(STATUS_LED, 1);
      //inside the address:
      if(iStatus.dataBitsRead < 8)
      {
        //we haven't read the 8 bits of the address yet:
        iStatus.address = (iStatus.address << 1) | receivedBit;
        iStatus.dataBitsRead++;
      }
      else
      {
        //we have read the address and are now waiting for a dcc zero:
        if(receivedBit == 0)
        {
          //a zero! 
          
          //now let's first compare the address with our own address:
          if((iStatus.address == myAddress) || (iStatus.address == 0))
          {
            //move to next state if it's our address or a broadcast address:
            iStatus.receiveState++;
            iStatus.dataBitsRead = 0; //reset the data bits counter for the next data byte
          }
          else
          {
            //not our address? not our data packet! Nothing to care about:
            iStatus.reset();
          }
        }
        else
        {
          //D'Oh! Start from the beginning...
          iStatus.reset();
        }
        
      }
      break;
    }
    
    case 3:
    {
      //inside the instruction byte
      if(iStatus.dataBitsRead < 8)
      {
        //we haven't read the 8 bits of the instruction yet:
        iStatus.instruction = (iStatus.instruction << 1) | receivedBit;
        iStatus.dataBitsRead++;
      }
      else
      {
        //DEBUG:
        executeCommand(iStatus.instruction);
        iStatus.reset();
        
        //NORMAL:
        
        //we have read the instruction and are now waiting for a dcc zero:
        /*if(receivedBit == 0)
        {
          //a zero! move to next state:
          iStatus.receiveState++;
          iStatus.dataBitsRead = 0; //reset the data bits counter for the next data byte
        }
        else
        {
          //D'Oh! Start from the beginning...
          iStatus.reset();
        }*/
        
      }
      break;
    }
    
    case 4: //WARNING: DEBUG ON!
    {
      //inside the error byte
      if(iStatus.dataBitsRead < 8)
      {
        //we haven't read the 8 bits of the error byte yet:
        iStatus.error = (iStatus.error << 1) | receivedBit;
        iStatus.dataBitsRead++;
      }
      else
      {
        //we have read the instruction and are now waiting for a dcc one:
        if(receivedBit == 1)
        {
          //a one! 
          
          //first calculate the error byte from address and instruction.
          //if it matches the received error byte the transmission was successful.
          
          unsigned char calculatedError = iStatus.address ^ iStatus.instruction;
          //NORMAL:
          //if(calculatedError == iStatus.error)
          //DEBUG:
          if(calculatedError = iStatus.error)
          {
            //Execute the command!
            executeCommand(iStatus.instruction);
          }
        }
        //Start from the beginning in every case!
        iStatus.reset();
        
      }
      break;
    }
    
  }
}



/** for interrupt method only: reacts on interrupt **/
void interruptReadBit()
{
  //DEBUG:
  digitalWrite(STATUS_LED, 1);
  
  delayMicroseconds(87); //1.5 times the high level of a dcc one
  if(digitalRead(DCCInputPin) == 0)
  {
    pushBit(1);
  }
  else
  {
    pushBit(0);
  }
  
  
}


/**
  This function handles the receiving of DCC bits.
  @return 0 for dcc zero bit, 1 for dcc one bit
**/
unsigned char receiveBit()
{
  
  unsigned short signalLength = 0;
  while(true)
  {
    //loop until the signal goes from HIGH to LOW
    if(digitalRead(DCCInputPin) == 1)
    {
      //count for how long the signal is on a HIGH level
      signalLength++;
    }
    else
    {
      //the signal has gone from HIGH to low.
      //Now we can detect if it is a DCC Zero or a DCC One:
      //if((signalLength == 3) || (signalLength == 4) || (signalLength == 5))
      if((signalLength == 3) || (signalLength == 4))
      {
        //we have detected a one
        return 1;
      }
      //else if(signalLength > 5)
      else if(signalLength > 4)
      {
        //we have detected a zero
        return 0;
      }
      else
      {
        //in all other cases reset the signalLength counter since we have read a too short impulse
        signalLength = 0;
      }
    }
    //delayMicroseconds(15);
    delayMicroseconds(19);
  }
}

/**
  This function detects DCC packets and checks their integrity and the address
  before passing the instruction byte to the executeCommand() function.
**/
void receiveCommand()
{
  unsigned char preambleCounter = 0;
  digitalWrite(DEBUG_LED, 0);
  
  while(true)
  {
    
    //digitalWrite(DEBUG_LED, 0);
    while(receiveBit() == 1)
    {
      //digitalWrite(DEBUG_LED, 1);
      preambleCounter++;
    }
    //digitalWrite(DEBUG_LED, 0);
    if(preambleCounter == 20)
    {
      //we have read the long preamble that is used for programming mode
      //To be implemented!
      digitalWrite(STATUS_LED, 1);
      digitalWrite(DEBUG_LED, 1);
      delay(500);
      digitalWrite(DEBUG_LED, 0);
    }
    else if(preambleCounter == 12)
    {
      digitalWrite(STATUS_LED, 0); //not ready to receive something
      digitalWrite(DEBUG_LED, 1);
      //we have read a valid preamble for operation mode
      //now we can read the bytes
      
      //check for the address byte start mark (a zero)
      if(receiveBit() == 0)
      {
        //address byte start mark detected, go on reading the address
        unsigned char address = 0;
        for(unsigned char i = 0; i < 8; i++)
        {
          address = (address << 1) | receiveBit(); //shift the address bits to the left and add a one or zero as LSB
        }
        
        //compare the address:
        if((address == myAddress) || address == 0)
        {
          digitalWrite(STATUS_LED, 1);
          digitalWrite(DEBUG_LED, 0);
          //the address is for me (either a packet for my address or a broadcast packet), go on receiving
          
          //check for the instruction byte start mark (a zero)
          if(receiveBit() == 0)
          {
            //instruction byte start mark detected, read the instruction byte
            unsigned char instruction = 0;
            for(unsigned char i = 0; i < 8; i++)
            {
              instruction = (instruction << 1) | receiveBit(); //shift the instruction bits to the left and add a one or zero as LSB
            }
            
            //digitalWrite(DEBUG_LED, 0);
            
            //now the last byte has to be read: the error detection data byte
            //procedure as always:
            //check for the error detection data byte start mark (a zero)
            if(receiveBit() == 0)
            {
              //error detection data byte start mark detected, read that byte
              unsigned char error = 0;
              for(unsigned char i = 0; i < 8; i++)
              {
                error = (error << 1) | receiveBit(); //shift the instruction bits to the left and add a one or zero as LSB
              }
              
              //receive a the closing bit (a one)
              if(receiveBit() == 1)
              {
                //the whole packet is read now. If we have reached it this far we can see if the packet was received correctly
                //and then call the executeCommand function.
                
                //first lets check if there were errors receiving the data:
                
                unsigned char calculatedError = address ^ instruction;
                if(calculatedError == error)
                {
                  digitalWrite(DEBUG_LED, 0);
                  digitalWrite(STATUS_LED,1);
                  //reception without errors
                  executeCommand(instruction);
                  //delay(100);
                  //digitalWrite(DEBUG_LED, 0);
                }
              }
            }
          }
          
        }
      }
      
      preambleCounter = 0; //reset the prefix counter
      digitalWrite(DEBUG_LED, 0);
      digitalWrite(STATUS_LED,1);
    }
    else
    {
      //invald prefix! start from the beginning!
      preambleCounter = 0;
      digitalWrite(DEBUG_LED, 0);
      digitalWrite(STATUS_LED,1);
    }
  }
}

/**
  Check for a address that was set manually via pins 7,8,9,10 and 11:
  values from 0 to 15 are possible. Pin 7 is the LSB, Pin 10 the MSB.
  Pin 11 must be HIGH to set an address manually.
  the address is written to memory only, not to EEPROM!
**/

void checkManualAddress()
{
  pinMode(11, INPUT); // the control pin
  pinMode(7, INPUT);
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  
  
  
  if(digitalRead(11) == 1)
  {
    //set address manually:
    unsigned char newAddress = 0;
    newAddress = (digitalRead(10) << 3);
    newAddress |= (digitalRead(9) << 2);
    newAddress |= (digitalRead(8) << 1);
    newAddress |= (digitalRead(7));
    myAddress = newAddress;
    
    //blink the LED to tell the least four bits of the address:
    
    //prefix:
    digitalWrite(STATUS_LED, 1);
    delay(500);
    digitalWrite(STATUS_LED,0);
    delay(500);
    digitalWrite(STATUS_LED, 1);
    delay(500);
    digitalWrite(STATUS_LED,0);
    delay(500);
    
    //the four bits:
    digitalWrite(STATUS_LED, ((newAddress & 0x0F) >> 3));
    delay(1000);
    digitalWrite(STATUS_LED, ((newAddress & 0x07) >> 2));
    delay(1000);
    digitalWrite(STATUS_LED, ((newAddress & 0x03) >> 1));
    delay(1000);
    digitalWrite(STATUS_LED, (newAddress & 0x01));
    delay(1000);
  }
  
  //manual address detection procedure end
  digitalWrite(STATUS_LED, 0);
  delay(250);
  digitalWrite(STATUS_LED,1);
  delay(250);
  digitalWrite(STATUS_LED, 0);
  delay(250);
  digitalWrite(STATUS_LED,1);
  delay(250);
  
  digitalWrite(STATUS_LED,0);
  delay(250);
}


void setup()
{
  //set pins:
  pinMode(motorForwardPin, OUTPUT);
  pinMode(motorReversePin, OUTPUT);
  pinMode(DCCInputPin, INPUT);
  pinMode(DEBUG_LED, OUTPUT);
  pinMode(STATUS_LED, OUTPUT); //blink the LED for status report
  
  //first check if the EEPROM contains a DCC configuration:
  unsigned char magic[4];
  magic[0] = EEPROM.read(0);
  magic[1] = EEPROM.read(1);
  magic[2] = EEPROM.read(2);
  magic[3] = EEPROM.read(3);
  
  //the magic code is "DCC\0". Check for that:
  if((magic[0] == 'D') && (magic[1] == 'C') && (magic[2] == 'C') && (magic[3] == '\0')) //to be optimized: replace with one uint32
  {
    //valid magic number
    myAddress = EEPROM.read(4); //read the address that is set in the EEPROM
    
  }
  else
  {
    //invalid magic number: initialize the EEPROM:
    EEPROM.write(0,'D');
    EEPROM.write(0,'C');
    EEPROM.write(0,'C');
    EEPROM.write(0,'\0');
    //EEPROM.write(0,0); //set address to 0 (analog mode)
    EEPROM.write(0,10); //set address to 10 
    myAddress = 10;
  }
  
  
  //checkManualAddress();
  
  //system operational message:
  digitalWrite(STATUS_LED,1);
  delay(100);
  digitalWrite(STATUS_LED,0);
  delay(500);
  digitalWrite(STATUS_LED,1);
  
    //----------------------
  //interrupt method: wait until the input pin raises to 1
  //and then wait for 87 microseconds.
  //If a 1 is read at that time we have received a dcc zero,
  //if a 0 is read at that time we have received a dcc one.
  
  iStatus.reset();
  
  //DEBUG:
  myAddress = 10;
  
  //PIN 2 (INT 0) is used here!
  attachInterrupt(0, interruptReadBit, RISING); 

}


void test_execute()
{
  //a test function for the executeCommand() function:
  executeCommand(0b01100010); //step 1 forward
  delay(2000);
  executeCommand(0b01100111); //step 7 forward
  delay(2000);
  executeCommand(0b01101111); //step 14 forward
  delay(5000);
  executeCommand(0b01100000); //stop
  delay(1000);
  
  executeCommand(0b01000010); //step 1 reverse
  delay(2000);
  executeCommand(0b01000111); //step 7 reverse
  delay(2000);
  executeCommand(0b01001111); //step 14 reverse
  delay(5000);
  executeCommand(0b01000000); //stop
  delay(1000);
  
}


void loop()
{
  //test_execute();
  
  /*while(true)
  {
    receiveCommand();
  }
  */
}
